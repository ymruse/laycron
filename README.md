# Cron表达式组件

#### 介绍
Layui自定义Cron表达式组件，生成cron表达式，目前没有校验，需要由后端接口校验数据。

#### 安装教程

1.  将 cron 文件夹放到扩展组件目录

#### 使用说明

1.  在使用页面引入模块，如：

```
layui
    .config({
      base: "../static/layui_exts/", // 扩展组件跟路径
    })
    .extend({
      cron: "cron/cron" // 扩展组件
    })
    .use(["cron"], function () {
      var $ = layui.$,
        cron = layui.cron;
        cron.render({
          elem: "#cron", // 绑定元素
		  run: "../static/json/run.json", // 获取最近运行时间的接口
		  done: function(cronStr) {
			console.log(cronStr);
		  }
      });
    });
```

2. 使用 render 方法绑定元素：

```
cron.render({
	elem: "#cron",                  // 绑定元素
	run: "../static/json/run.json", // 获取最近运行时间的接口
	done: function(cronStr) {       // 点击确定，触发，参数为 cron 表达式字符串
		console.log(cronStr);
	}
});
```

```
<input type="text" class="layui-input" id="cron" value="0/20 * * * * ?" />
```

3. 效果截图：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0421/233924_7b1d15a8_1163950.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0421/234000_31553e32_1163950.png "屏幕截图.png")